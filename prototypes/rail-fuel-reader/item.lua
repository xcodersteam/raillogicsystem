data:extend(
{ 
	{
		type = "item",
		name = "rail-fuel-reader",
		icon = "__RailLogicSystem__/graphics/rail-fuel-reader-icon.png",
		flags = {"goes-to-quickbar"},
		subgroup = "rail-logic-system-input",
		order = "a[input]",
		place_result = "rail-fuel-reader",
		stack_size = 50
  }
}) 

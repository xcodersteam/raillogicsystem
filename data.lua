data:extend({
    {
        type = "custom-input",
        name = "rail-logic-system-open-gui",
        key_sequence = "SHIFT + G",
        consuming = "script-only"
    }
})

require("item-group")

require("prototypes.rail-station-controller.entity")
require("prototypes.rail-station-controller.item")
require("prototypes.rail-station-controller.recipe")

require("prototypes.rail-cargo-reader.entity")
require("prototypes.rail-cargo-reader.item")
require("prototypes.rail-cargo-reader.recipe")

require("prototypes.rail-fuel-reader.entity")
require("prototypes.rail-fuel-reader.item")
require("prototypes.rail-fuel-reader.recipe")

require("prototypes.tech")


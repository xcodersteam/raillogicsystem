data:extend({
    {
        type = "lamp",
        name = "rail-station-controller",
        icon = "__RailLogicSystem__/graphics/rail-station-controller/icon.png",
        flags = { "placeable-neutral", "player-creation" },
        minable = { hardness = 0.2, mining_time = 0.5, result = "rail-station-controller" },
        max_health = 55,
        corpse = "small-remnants",
        collision_box = { { -0.15, -0.15 }, { 0.15, 0.15 } },
        selection_box = { { -0.5, -0.5 }, { 0.5, 0.5 } },
        vehicle_impact_sound = { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
        energy_source =
        {
            type = "electric",
            usage_priority = "secondary-input"
        },
        energy_usage_per_tick = "10KW",
        light = { intensity = 0, size = 0 },
        picture_off =
        {
            filename = "__RailLogicSystem__/graphics/rail-station-controller/light-off.png",
            priority = "high",
            width = 67,
            height = 58,
            frame_count = 1,
            axially_symmetrical = false,
            direction_count = 1,
            shift = { -0.015625, 0.15625 }
        },
        picture_on =
        {
            filename = "__RailLogicSystem__/graphics/rail-station-controller/light-on-patch.png",
            priority = "high",
            width = 62,
            height = 62,
            frame_count = 1,
            axially_symmetrical = false,
            direction_count = 1,
            shift = { -0.03125, -0.03125 }
        },
        circuit_wire_connection_point =
        {
            shadow =
            {
                red = { 0.734375, 0.578125 },
                green = { 0.609375, 0.640625 },
            },
            wire =
            {
                red = { 0.40625, 0.34375 },
                green = { 0.40625, 0.5 },
            }
        },
        circuit_wire_max_distance = 7.5
    }
})
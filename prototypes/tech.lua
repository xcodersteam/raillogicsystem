data:extend(
{
  {
    type = "technology",
    name = "rail-logic-system",
    icon = "__RailLogicSystem__/graphics/tech.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "rail-station-controller"
      },
      {
        type = "unlock-recipe",
        recipe = "rail-cargo-reader"
      },
      {
        type = "unlock-recipe",
        recipe = "rail-fuel-reader"
      }
    },
    prerequisites = {"circuit-network","railway","rail-signals"},
    unit =
    {
      count = 50,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 15
    },
    order = "a-d-d",
  }
})
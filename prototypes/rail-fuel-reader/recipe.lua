data:extend({
    {
        type = "recipe",
        name = "rail-fuel-reader",
        enabled = false,
        ingredients = { { "copper-cable", 4 }, { "constant-combinator", 1 }, { "advanced-circuit", 2 } },
        result = "rail-fuel-reader",
        result_count = 1
    }
})
require("util")

local data = {}
local classes = {}
local gui = {}

local function ttostring(tab)
    local res = ""
    if type(tab) == "table" then
        res = "{"
        for k, v in pairs(tab) do
            res = res .. " " .. tostring(k) .. " = " .. ttostring(v) .. ","
        end
        res = res .. "}"
    else
        res = tostring(tab)
    end
    return res
end

local function getArea(pos, size)
    size = size or 1
    return { { pos.x - size, pos.y - size }, { pos.x + size, pos.y + size } }
end

local function getTrain(entity)
    return entity.train
end

local function save()
    global.railLogicSystem = data
end

local function CloseSCGUI(playerIndex)
    if gui[playerIndex] == nil then
        return
    end
    data["rail-station-controller"][gui[playerIndex].entityIndex].station = gui[playerIndex].gui.station.text
    gui[playerIndex].gui.destroy()
    gui[playerIndex] = nil
    save()
end

local function OpenSCGUI(playerIndex, entityIndex, player)
    if gui[playerIndex] ~= nil then
        CloseSCGUI(playerIndex)
    end
    local g = player.gui.center.add({ type = "frame", name = "station_selection", direction = "vertical" })
    gui[playerIndex] = { gui = g, player = player, entityIndex = entityIndex }
    g.add({ type = "label", name = "test", caption = "Station name" })
    g.add({ type = "textfield", name = "station", text = data["rail-station-controller"][entityIndex].station })
    g.add({ type = "button", name = "rail_logic_system_close", caption = { "gui.close" }, style = "button_style" })
end

classes["rail-station-controller"] = {
    onPlace = function(entity)
        return { entity = entity, station = "" }
    end,
    onDestroy = function(entity) end,
    onTick = function(obj, index)
        local controll = obj.entity.get_control_behavior()
        if controll and (controll.circuit_condition.fulfilled or controll.logistic_condition.fulfilled) and obj.entity.energy > 0.5 then
            for i, k in ipairs(obj.entity.surface.find_entities(getArea(obj.entity.position))) do
                local ok, train = pcall(getTrain, k)
                if ok then
                    for i, k in ipairs(train.schedule.records) do
                        if tostring(k.station) == tostring(obj.station) then
                            local sch = train.schedule
                            if sch.current ~= i then
                                sch.current = i
                                train.schedule = sch
                                if not train.manual_mode then
                                    train.manual_mode = true
                                    train.manual_mode = false
                                end
                                break
                            end
                        end
                    end
                    break
                end
            end
        end
    end
}

--Train Inventory Reader
classes["rail-cargo-reader"] = {
    onPlace = function(entity) return { entity = entity } end,
    onDestroy = function() end,
    onTick = function(k)
        local inv = {}
        for i, k in ipairs(k.entity.surface.find_entities(getArea(k.entity.position))) do
            local ok, train = pcall(getTrain, k)
            if ok then
                for x, y in pairs(train.cargo_wagons) do
                    for k, v in pairs(y.get_inventory(1).get_contents()) do
                        inv[k] = (inv[k] or 0) + v
                    end
                end
                break
            end
        end

        local i = 1
        local out = {}
        for k, v in pairs(inv) do
            out[i] = { signal = { type = "item", name = k }, count = v, index = i }
            i = i + 1
            if i > 60 then break end
        end
        k.entity.get_control_behavior().parameters = { parameters = out }
    end
}

--Train Fuel Reader
classes["rail-fuel-reader"] = {
    onPlace = function(entity) return { entity = entity } end,
    onDestroy = function() end,
    onTick = function(k)
        local inv = {}
        for i, k in ipairs(k.entity.surface.find_entities(getArea(k.entity.position))) do
            local ok, train = pcall(getTrain, k)
            if ok then
                for x, y in pairs(train.locomotives) do
                    for q, w in pairs(y) do
                        for k, v in pairs(w.get_inventory(1).get_contents()) do
                            inv[k] = (inv[k] or 0) + v
                        end
                    end
                end
                break
            end
        end

        local i = 1
        local out = {}
        for k, v in pairs(inv) do
            out[i] = { signal = { type = "item", name = k }, count = v, index = i }
            i = i + 1
            if i > 60 then break end
        end
        k.entity.get_control_behavior().parameters = { parameters = out }
    end
}

local function entityBuilt(event)
    if classes[event.created_entity.name] ~= nil then
        local tab = data[event.created_entity.name]
        table.insert(tab, classes[event.created_entity.name].onPlace(event.created_entity))
        data[event.created_entity.name] = tab
        save()
    end
end

local function entityRemoved(event)
    if classes[event.entity.name] ~= nil then
        for k, v in ipairs(data[event.entity.name]) do
            if v.entity == event.entity then
                local tab = data[event.entity.name]
                table.remove(tab, k)
                classes[event.entity.name].onDestroy(v)
                data[event.entity.name] = tab
                save()
                break
            end
        end
    end
end

local function onTick()
    for k, v in pairs(classes) do
        for q, i in pairs(data[k]) do
            if i.entity.valid then
                v.onTick(i, q)
            end
        end
    end
end

local function onLoad()
    data = global.railLogicSystem or {}
    for k, v in pairs(classes) do
        data[k] = data[k] or {}
    end
end

script.on_init(onLoad)
script.on_load(onLoad)

script.on_event(defines.events.on_tick, onTick)

script.on_event(defines.events.on_built_entity, entityBuilt)
script.on_event(defines.events.on_robot_built_entity, entityBuilt)

script.on_event(defines.events.on_preplayer_mined_item, entityRemoved)
script.on_event(defines.events.on_robot_pre_mined, entityRemoved)
script.on_event(defines.events.on_entity_died, entityRemoved)
script.on_event("rail-logic-system-open-gui", function(event)
    local player = game.players[event.player_index]
    local entity = player.selected
    if entity and entity.name == "rail-station-controller" and player.can_reach_entity(entity) then
        for k, v in pairs(data["rail-station-controller"]) do
            if v.entity == entity then
                OpenSCGUI(event.player_index, k, player)
                break
            end
        end
    else
        CloseSCGUI(event.player_index)
    end
end)
script.on_event(defines.events.on_gui_click, function(event)
    local player = game.players[event.player_index]
    if event.element.name == "rail_logic_system_close" then
        CloseSCGUI(event.player_index)
    end
end)